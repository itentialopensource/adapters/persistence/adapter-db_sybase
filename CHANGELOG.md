
## 0.3.5 [10-15-2024]

* Changes made at 2024.10.14_20:37PM

See merge request itentialopensource/adapters/adapter-db_sybase!10

---

## 0.3.4 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-db_sybase!8

---

## 0.3.3 [08-14-2024]

* Changes made at 2024.08.14_18:52PM

See merge request itentialopensource/adapters/adapter-db_sybase!7

---

## 0.3.2 [08-07-2024]

* Changes made at 2024.08.07_10:53AM

See merge request itentialopensource/adapters/adapter-db_sybase!6

---

## 0.3.1 [07-26-2024]

* manual updates

See merge request itentialopensource/adapters/persistence/adapter-db_sybase!4

---

## 0.3.0 [01-06-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/persistence/adapter-db_sybase!1

---

## 0.2.1 [04-05-2021]

* Bug fixes and performance improvements

See commit 1be9196

---
