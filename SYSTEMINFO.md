# Sybase SQL

Vendor: SAP
Homepage: https://www.sap.com/index.html

Product: Sybase
Product Page: https://www.sap.com/products/technology-platform/sybase-ase.html

## Introduction
We classify Sybase into the Data Storage domaina as Sybase is a database which provides the storage of information. 

"Deploy on premises and on an infrastructure as a service (IaaS)"
"Deliver high performance and availability to process mission-critical transactions"
"Provide a proven, flexible SQL database system to reduce risk and increase agility"
"Lower operational costs with a resource-efficient relational database server"

## Why Integrate
The Sybase adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SAP Sybase (Adaptive Server Enterprise - ASE). With this adapter you have the ability to perform operations with Sybase on items such as:

- Storage of Information
- Retrieval of Information

## Additional Product Documentation
The [Sybase SQL Reference](https://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.help.sqlanywhere.12.0.1/dbusage/sqlqueries-sqlug.html)
The [Sybase Node Library Documentation](https://www.npmjs.com/package/sybase)