SAP Sybase Adapter
===========

This adapter allows interaction with a Sybase server. For efficiency, this adapter should only be used in IAP workflow calls. Calling the adapter from Applications instead of using the npm sybase pacakge will be less efficient!

Notes:
1. If IAP is running in docker, and Sybase server is running in the local machine, host in adapter properties should be 'host.docker.internal' instead of 'localhost' or '127.0.0.1'.

License & Maintainers
---

### Maintained by:

Itential Adapter Team (<product_team@itential.com>)

Check the [changelog](CHANGELOG.md) for the latest changes.

### License

Itential, LLC proprietary
